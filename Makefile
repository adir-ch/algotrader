#--- Makefile ----

BINARY_DIR=cmd
TESTS_DIR=tests
GOCMD=go
GOBUILD=$(GOCMD) build
GOTEST=gotest

BINARY_NAME=algotrader
BINARY_LINUX=$(BINARY_NAME)
BINARY_WINDOWS=$(BINARY_NAME).exe
INTEGRATION_SRV=integration-srv
VERSION=`git describe --abbrev=0 --tags`
BUILD=`date +%FT%T%z`

# Setup the -ldflags option for go build here, interpolate the variable values
LDFLAGS=-ldflags "-X main.Version=${VERSION} -X main.Build=${BUILD}"

all: show-ver-tag test build_linux64

show-ver-tag:
	@echo "${VERSION}"

build_windows:
	CC=x86_64-w64-mingw32-gcc CXX=x86_64-w64-mingw32-g++ CGO_ENABLED=1 GOOS=windows GOARCH=amd64 \
	$(GOBUILD) ${LDFLAGS} -o ./${BINARY_DIR}/windows/64/${BINARY_WINDOWS} $(BINARY_DIR)/main.go

build_linux: build_linux64 build_linux32

build_linux32:
	CGO_ENABLED=1 GOOS=linux GOARCH=386 $(GOBUILD) ${LDFLAGS} -o ./${BINARY_DIR}/linux/32/${BINARY_LINUX}

build_linux64:
	GO111MODULE=on CGO_ENABLED=1 GOOS=linux GOARCH=amd64 $(GOBUILD) ${LDFLAGS} -o ./${BINARY_DIR}/linux/64/${BINARY_LINUX} $(BINARY_DIR)/main.go

test:
	$(GOTEST) -v -cover ./pkg/...

integration: 
	$(GOBUILD) -o ./${BINARY_DIR}/linux/64/${INTEGRATION_SRV} $(TESTS_DIR)/integration.go
	./$(TESTS_DIR)/run-integration.sh -d $(TESTS_DIR)/data.json

clean:
	rm -rf $(BINARY_DIR)/windows
	rm -rf $(BINARY_DIR)/linux