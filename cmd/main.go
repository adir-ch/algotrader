package main

import (
	"io"
	"os"

	env "github.com/caarlos0/env/v6"
	log "github.com/sirupsen/logrus"
	"gitlab.com/adir-ch/algotrader/pkg/trader"
)

type config struct {
	Env           string `env:"Environment"`
	BuySellOpTime int    `env:"BuySellOpTime" envDefault:"1"`
}

var (
	// Version - the trader version
	Version string

	// Build - the trader build ID
	Build string

	mainLogger   *log.Entry
	traderLogger *log.Entry

	cfg config
)

func init() {
	log.SetOutput(os.Stdout)

	mainLogger = log.WithFields(log.Fields{"main": "init"})
	//traderLogger = log.WithFields(log.Fields{"trader": "trading"})

	cfg = config{}
	if err := env.Parse(&cfg); err != nil {
		mainLogger.Panicf("Unable to read configuration %s\n", err)
	}

	if cfg.Env == "Production" {
		log.SetFormatter(&log.JSONFormatter{})
		log.SetReportCaller(false)
		log.SetLevel(log.InfoLevel)
		log.SetOutput(io.MultiWriter(os.Stdout, os.Stderr))
	} else {
		log.SetFormatter(&log.TextFormatter{}) // not really needed
		//log.SetReportCaller(true)
		log.SetLevel(log.TraceLevel)
	}

	mainLogger.Tracef("configuration loaded: %+v\n", cfg)
}

func main() {
	mainLogger.Infof("AlgoTrader starting build: %s, ver: %s", Build, Version)

	input := []int{10, 7, 5, 8, 11, 9}

	var v trader.Validator = trader.SimpleValidator{
		MinStockPrice: 0,
		MinInputLen:   trader.GetMinimalInputLen(cfg.BuySellOpTime),
	}

	validatorDecorators := []trader.ValidatorDecorator{
		trader.GetMaxLenDecorator((60 * 24)),

		// additional decorators...
	}

	for _, decorator := range validatorDecorators {
		v = decorator(v)
	}

	maxProfit, err := trader.Trade(input, cfg.BuySellOpTime, v, trader.FindMaxProfit)
	if err != nil {
		mainLogger.Errorf("error while trading: %s", err)
		return
	}

	mainLogger.Debugf("input: %v => max profit = %d", input, maxProfit)
}
