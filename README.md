# AlgoTrader

A Bot trading shares

### Scenario

Suppose we could access yesterday's stock prices as a list, where:

* The indices are the time in minutes past trade opening time, which was 10:00am local time.
* The values are the price in dollars of the Latitude Financial stock at that time.
* So if the stock cost $5 at 11:00am, stock_prices_yesterday[60] = 5.

Write an *efficient function* that takes an array of stock prices and returns the best profit I could have made from 1 purchase and 1 sale of 1 Latitude Financial stock yesterday.

For example:
```js
var stock_prices_yesterday = [10, 7, 5, 8, 11, 9];

get_max_profit(stock_prices_yesterday)
# returns 6 (buying for $5 and selling for $11)
```

You must buy before you sell.
You may not buy and sell in the same time step (at least 1 minute must pass).

## Expectations
* implement a solution in any language
* provide code that runs and prove it works (i.e. show something that can be run / or automated tests)
* include any comments that you think will be relevant to provide any context around the approach taken / solution developed

## Assumptions / Design decitions 
* All stock prices are always bigger than 0
* Stock prices are represented as int
* It is possible to sell in the last minute of the day to get max profit. 
* A validator is implemented only for the input stocks data, as an example

## Building and running the tests 

### Building for your OS 
    bash# make build_linux64 - will build the code for a Linux 64bit machine 
    bash# make build_linux32 - will build the code for a Linux 32bit machine 
    bash# make build_window - will build the code for a Windows 64bit machine 

### Running the unit-tests 
    bash# make test - will run all unit-tests and will show the current code cover

### Running the integration tests
The integration tests data can be found on the tests folder in the <b>data.json</b> file. To run the tests:

    bash# make integration - will run the integration tests 

Sample output 

    Serving integration test request
    running integration test with data: {Name:example test OpWaitTime:1 StocksData:[10 7 5 8 11 9] Result:6}
    INFO[0001] starting to trade: op-time: 1, input: [10 7 5 8 11 9]  trader=trading
    trader result: got: 6, expected: 6
