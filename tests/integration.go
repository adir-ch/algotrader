package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/adir-ch/algotrader/pkg/trader"
)

type testData struct {
	Name       string `json:"name,omitempty"`
	OpWaitTime int    `json:"op_wait_time,omitempty"`
	StocksData []int  `json:"stocks_data,omitempty"`
	Result     int    `json:"result,omitempty"`
}

func inegrationTestHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Serving integration test request")
	var requestBodyData []byte
	var err error
	if requestBodyData, err = ioutil.ReadAll(r.Body); err != nil {
		fmt.Printf("cannot read data from request. Error: %s\n", err)
		return
	}

	requestData := testData{}
	err = json.Unmarshal(requestBodyData, &requestData)
	if err != nil {
		w.Write([]byte("Error while parsing test data"))
		return
	}

	fmt.Printf("running integration test with data: %+v\n", requestData)

	traderResult := trade(&requestData)
	w.Write([]byte(fmt.Sprintf("trader result: got: %d, expected: %d", traderResult, requestData.Result)))
}

func trade(data *testData) int {
	var v trader.Validator = trader.SimpleValidator{
		MinStockPrice: 0,
		MinInputLen:   trader.GetMinimalInputLen(data.OpWaitTime),
	}

	validatorDecorators := []trader.ValidatorDecorator{
		trader.GetMaxLenDecorator((60 * 24)),

		// additional decorators...
	}

	for _, decorator := range validatorDecorators {
		v = decorator(v)
	}

	maxProfit, err := trader.Trade(data.StocksData, data.OpWaitTime, v, trader.FindMaxProfit)
	if err != nil {
		fmt.Printf("error while trading: %s\n", err)
		return -1
	}

	return maxProfit
}

func main() {
	http.HandleFunc("/integration", inegrationTestHandler)
	fmt.Println("listening for integration tests data on port 8080")
	http.ListenAndServe(":8080", nil)
}
