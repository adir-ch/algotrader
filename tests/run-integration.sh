#! /bin/bash

option=$1
data=$2
echo "running with $option $data"

function Usage {
	echo "Usage: $0 <option> <data>"
	echo " Option: -d <data>: parse data from file"
}

function postData {
    curl -s -d "$1" -X POST localhost:8080/integration
    echo
    echo "##########################"
}

if [ "$option" != "-d" ] 
then
	Usage
	exit
fi

if [ -z $data ]; then
	echo "data not found"
	Usage
	exit
fi

echo "running integration server"

./cmd/linux/64/integration-srv &
echo "waiting for integration server..."
sleep 2

if [ "$option" == "-d" ]; then
	cat $data | egrep -v '(^$|^#)' | while read line; do
  		postData "$line"
		echo ""
  	done
fi

sleep 2
echo "======> done running integration tests - stopping server"
killall -9 integration-srv

