package trader

import "fmt"

// Validator - input data validator
type Validator interface {
	Validate(input []int) error
}

//ValidatorFunc - validator func
type ValidatorFunc func(input []int) error

// Validate - interface implementation
func (l ValidatorFunc) Validate(input []int) error {
	return l(input)
}

// ValidatorDecorator - a validator decorator extension
type ValidatorDecorator func(Validator) Validator

// SimpleValidator - validate minimal stock price
type SimpleValidator struct {
	MinStockPrice int
	MinInputLen   int
}

// Validate - validate min price
func (v SimpleValidator) Validate(input []int) error {
	if len(input) < v.MinInputLen {
		return fmt.Errorf("not enough stocks to trade with found: %d, need: %d", len(input), v.MinInputLen)
	}

	for i, p := range input {
		if p < v.MinStockPrice {
			return fmt.Errorf("illegal price was found: minute: %d, price: %d", i, p)
		}
	}
	tradeLogger.Debugf("stocks input data validated successfully")
	return nil
}

// GetMaxLenDecorator - a validator decorator to validate the input data lenght
func GetMaxLenDecorator(maxLen int) ValidatorDecorator {
	return func(v Validator) Validator {
		f := func(input []int) error {
			if len(input) > maxLen {
				return fmt.Errorf("input data is too long: expected %d, found: %d", maxLen, len(input))
			}
			return v.Validate(input)
		}

		return ValidatorFunc(f)
	}
}
