package trader

import (
	"testing"
)

func TestSimpleValidator_Validate(t *testing.T) {
	type fields struct {
		MinStockPrice int
		MinInputLen   int
	}
	type args struct {
		input []int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{"validation error on empty input", fields{0, 3}, args{[]int{}}, true},
		{"validation error on short input", fields{0, 3}, args{[]int{1, 2}}, true},
		{"validation error on illegal stock price in input", fields{3, 3}, args{[]int{4, 7, 9, 11, 22, 2, 8}}, true},
		{"no validatoion error", fields{3, 3}, args{[]int{4, 7, 9, 11, 22, 14, 8}}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := SimpleValidator{
				MinStockPrice: tt.fields.MinStockPrice,
				MinInputLen:   tt.fields.MinInputLen,
			}
			if err := v.Validate(tt.args.input); (err != nil) != tt.wantErr {
				t.Errorf("SimpleValidator.Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMaxLenDecorator(t *testing.T) {

	validate := func(input []int) error {
		return nil
	}

	type args struct {
		input  []int
		maxLen int
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"no validation error on empty input", args{[]int{}, 20}, false},
		{"no validation error on empty input and 0 max len", args{[]int{}, 0}, false},
		{"no validation error on input len equal max len", args{[]int{1, 2, 3, 4, 5}, 5}, false},
		{"validation error on input len larger than max len", args{[]int{1, 2, 3, 4, 5}, 4}, true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := GetMaxLenDecorator(tt.args.maxLen)
			v := d(ValidatorFunc(validate))
			if err := v.Validate(tt.args.input); (err != nil) != tt.wantErr {
				t.Errorf("MaxLenDecorator.Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
