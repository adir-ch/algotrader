package trader

import (
	"errors"
	"testing"
)

func TestGetMinimalInputLen(t *testing.T) {
	type args struct {
		opWaitTime int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"wait time between ops illegal", args{-1}, 2},
		{"wait time between ops is zero", args{0}, 2},
		{"large wait time between ops", args{500}, 502},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetMinimalInputLen(tt.args.opWaitTime); got != tt.want {
				t.Errorf("GetMinimalInputLen() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getTradeStopTime(t *testing.T) {
	type args struct {
		input           []int
		buySellWaitTime int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"no input", args{[]int{}, 2}, 0},
		{"illegal operation time", args{[]int{1, 2, 3}, -2}, 0},
		{"no operation wait time", args{[]int{1, 2, 3, 4, 5}, 4}, 0},
		{"no operation wait time with short inpu", args{[]int{1}, 0}, 0},
		{"not enough data to trade", args{[]int{1, 2, 3}, 2}, 0},
		{"by sell time time is too high to make any trades", args{[]int{1, 2, 3, 4, 5}, 7}, 0},
		{"can make only one trade", args{[]int{1, 2, 3, 4, 5}, 3}, 1},
		{"can trade up to minute 5", args{[]int{1, 2, 3, 4, 5, 6, 7, 8, 9}, 2}, 6},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getTradeStopTime(tt.args.input, tt.args.buySellWaitTime); got != tt.want {
				t.Errorf("getTradeStopTime() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_FindMaxProfit(t *testing.T) {
	type args struct {
		baseTime  int
		startFrom int
		input     []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"no data to trade", args{3, 5, []int{}}, 0},
		{"not enough data to trade", args{3, 5, []int{1, 2}}, 0},
		{"no change in data", args{0, 2, []int{1, 1, 1, 1, 1, 1, 1}}, 0},
		{"price is only dropping", args{0, 2, []int{9, 8, 7, 6, 5, 4, 3}}, 0},
		{"price goes up but stay steady", args{0, 1, []int{1, 2, 3, 3, 3, 3, 3, 3}}, 2},
		{"max profit on minute 5", args{0, 1, []int{1, 2, 3, 3, 3, 10, 3, 3}}, 9},
		{"max profit on last minute of the day", args{0, 1, []int{1, 2, 3, 3, 3, 10, 3, 22}}, 21},
		{"max profit on second minute of the day", args{0, 1, []int{1, 30, 3, 3, 3, 10, 3, 22}}, 29},
		{"missed max profit", args{0, 2, []int{1, 30, 3, 3, 3, 3}}, 2},
		{"example data", args{2, 4, []int{10, 7, 5, 8, 11, 9}}, 6},
		{"example data 2", args{2, 4, []int{100, 7, 5, 8, 11, 9}}, 6},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FindMaxProfit(tt.args.baseTime, tt.args.startFrom, tt.args.input); got != tt.want {
				t.Errorf("findMaxProfit() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTrade(t *testing.T) {
	type args struct {
		input           []int
		buySellWaitTime int
		validatorRet    error
		profiterRet     int
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{"data is not valid", args{[]int{}, 1, errors.New("validation error"), 0}, 0, true},
		{"operation wait time is too long to trade", args{[]int{1, 2, 3, 4}, 5, nil, 0}, 0, false},
		{"no need to wait on buy or sell", args{[]int{1, 2, 3, 4}, 0, nil, 3}, 3, false},
		{"data is valid but no profit", args{[]int{1, 1, 1, 1}, 1, nil, 0}, 0, false},
		{"example data", args{[]int{10, 7, 5, 8, 11, 9}, 1, nil, 6}, 6, false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := func(input []int) error {
				return tt.args.validatorRet
			}

			p := func(basePrice int, stratFromTime int, stocksData []int) int {
				return tt.args.profiterRet
			}

			got, err := Trade(tt.args.input, tt.args.buySellWaitTime, ValidatorFunc(v), p)
			if (err != nil) != tt.wantErr {
				t.Errorf("Trade() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Trade() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_max(t *testing.T) {
	type args struct {
		left  int
		right int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"left > right", args{10, 5}, 10},
		{"left < right", args{10, 15}, 15},
		{"left = right", args{10, 10}, 10},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := max(tt.args.left, tt.args.right); got != tt.want {
				t.Errorf("max() = %v, want %v", got, tt.want)
			}
		})
	}
}
