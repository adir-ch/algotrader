package trader

import (
	log "github.com/sirupsen/logrus"
)

// ProfitFunc - profit calculator type
type ProfitFunc func(basePrice int, stratFromTime int, stocksData []int) int

var tradeLogger *log.Entry

func init() {
	tradeLogger = log.WithFields(log.Fields{"trader": "trading"})
}

// Trade - for a given stocks prices by time will find maximum profit
func Trade(input []int, buySellWaitTime int, validator Validator, profiter ProfitFunc) (int, error) {
	if err := validator.Validate(input); err != nil {
		return 0, err
	}

	tradeLogger.Infof("starting to trade: op-time: %d, input: %v", buySellWaitTime, input)
	stopAtTime := getTradeStopTime(input, buySellWaitTime)
	tradeLogger.Debugf("can maximize profit from start - minute %d", stopAtTime)

	maxProfit := 0
	for minute := 0; minute < stopAtTime; minute++ {
		p := profiter(minute, (minute + 1 + buySellWaitTime), input)
		maxProfit = max(maxProfit, p)
	}
	return maxProfit, nil
}

// GetMinimalInputLen - returns the minimal input data len for successful trading
func GetMinimalInputLen(opWaitTime int) int {
	if opWaitTime < 0 {
		tradeLogger.Warnf("illegal opWaitTime %d, will configure wait time between buy and sell to: 0", opWaitTime)
		return 2
	}

	tradeLogger.Debugf("min input data len: %d", (opWaitTime + 2))
	return (opWaitTime + 2)
}

// FindMaxProfit - search for a max profit given the stock base price and the stocks day
// data from a specific time in day
func FindMaxProfit(baseTime, tradeStartTime int, stockData []int) int {
	maxProfit := 0
	len := len(stockData)

	if tradeStartTime > (len - 1) {
		tradeLogger.Errorf("illegal offset %d found, while max len is: %d - aborting", tradeStartTime, len)
		return 0 // no profit
	}

	for i := tradeStartTime; i < len; i++ {
		if stockData[baseTime] < stockData[i] {
			profit := (stockData[i] - stockData[baseTime])
			maxProfit = max(maxProfit, profit)
		}
	}

	tradeLogger.Tracef("found max profit for base: %d at minute %d = %d", stockData[baseTime], baseTime, maxProfit)
	return maxProfit
}

func getTradeStopTime(input []int, buySellWaitTime int) int {
	if buySellWaitTime < 0 {
		tradeLogger.Errorf("illegal buy sell wait time: %d", buySellWaitTime)
		return 0
	}

	stopTime := len(input) - buySellWaitTime - 1
	if stopTime < 0 {
		return 0
	}
	return stopTime
}

func max(left, right int) int {
	if left > right {
		return left
	}
	return right
}
