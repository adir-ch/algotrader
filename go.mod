module gitlab.com/adir-ch/algotrader

go 1.12

require (
	github.com/caarlos0/env/v6 v6.0.0
	github.com/sirupsen/logrus v1.4.2
)
